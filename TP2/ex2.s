.data
msg:
	.byte 42
buff:
	.space 2
.text
.globl _start
_start:
	mov $3, %rax
	mov $0, %rbx
	mov $buff, %rcx
	mov $1, %rdx
	int $0x80

	mov $buff, %r10
	movb (%r10), %r8b
	mov $48, %r9
	sub %r9, %r8
	jz end

loopstart:
	push %r8
	mov $4, %rax
	mov $1, %rbx
	mov $msg, %rcx
	mov $1, %rdx
	int $0x80

	pop %r8
	dec %r8
	jnz loopstart
	
end:
	mov $0, %rbx
	mov $1, %rax
	int $0x80
