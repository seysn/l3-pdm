TP2
===

- ex2.s
  Ecrire un programme qui lit au clavier un entier N codé sur un caractère et affiche une ligne de N caractères '*'

- ex2-2.s
  Ecrire un programme qui lit au clavier un entier N codé sur plusieurs caractères

- ex2-3.s
  Ecrire un programme qui lit au clavier un entier N codé sur plusieurs caractères et affiche un carré composé de '*' de côté N

- ex2-3bis.s
  Programme que j'ai obtenu par pur hasard qui fait un triangle au lieu d'un carré  
