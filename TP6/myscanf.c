#include <stdio.h>

void my_scanf(char*, void*);

int main(void) {
	char s[64];
	char c;
	int i;

	printf("Input (char[64]): ");
	fflush(0);
	my_scanf("%s", &s);
	printf("String : %s\n", s);

	printf("Input (char): ");
	fflush(0);
	my_scanf("%c", &c);
	printf("Character : %c\n", c);

	printf("Input (int): ");
	fflush(0);
	my_scanf("%d", &i);
	printf("Number : %d\n", i);

	return 0;
}
