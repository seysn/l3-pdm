	.data
buff:
	.space 1024
charbuff:
	.space 1
	
	.text
	.globl my_scanf
	.type my_scanf, @function
my_scanf:
	mov $0, %r8
	movb (%rdi), %r8b
	cmp $37, %r8	
	je if_percent	/* if '%' */
	cmp $0, %r8
	je end
	addq $1, %rdi
	jmp my_scanf
if_percent:
	addq $1, %rdi
	mov $0, %r8
	movb (%rdi), %r8b
	push %rdi
	cmp $99, %r8			
	je readchar_call	/* if 'c' */
	cmp $100, %r8 			
	je readnumber_call	/* if 'd' */
	cmp $115, %r8			
	je readstring_call	/* if 's' */
	
/* EXITS */
	
my_scanf_exit:
	mov %rax, (%rsi)
	pop %rdi
	jmp my_scanf
my_scanf_exit_int:
	mov %rax, (%rsi)
	pop %rdi
	jmp my_scanf	
my_scanf_exit_str:
	movb $0, (%rsi)/* Add '\0' to the end of string*/
	pop %rdi
	jmp my_scanf
end:
	ret

/* CALLS */
	
readchar_call:
	call readchar
	jmp my_scanf_exit
	
readnumber_call:
	call readnumber
	jmp my_scanf_exit_int

readstring_call:
	call readstring
cpy_str_loop:
	movb (%rax), %bl
	cmp $0, %bl
	je my_scanf_exit_str
	movb %bl, (%rsi)
	addq $1, %rax
	addq $1, %rsi
	jmp cpy_str_loop

/* READCHAR */
	
	.globl readchar
	.type readchar, @function
readchar:
	/* Read one byte */
	mov $3, %rax
	mov $0, %rbx
	mov $charbuff, %rcx
	mov $1, %rdx
	int $0x80
	/* Clean the buffer */
	mov $3, %rax
	mov $0, %rbx
	mov $buff, %rcx
	mov $1024, %rdx
	int $0x80
	/* Return the char */
	mov $charbuff, %r10
	mov (%r10), %rax
	ret

/* READSTRING */
	
	.globl readstring
	.type readstring, @function
readstring:
	mov $3, %rax
	mov $0, %rbx
	mov $buff, %rcx
	mov $1024, %rdx
	int $0x80
	mov $buff, %r10
rm_newline_loop:
	movq %r10, %rcx
	movb (%rcx), %bl
	cmp $10, %bl
	je rm_newline_end 	/* if '\n' */
	addq $1, %r10
	jmp rm_newline_loop
rm_newline_end:
	movb $0, (%r10)
	mov $buff, %rax
	ret
	
/* READNUMBER */

	.globl readnumber
	.type readnumber, @function
readnumber:
	call readstring
	mov %rax, %rdi
	mov $0, %rax
rn_loop:
	movq %rdi, %rcx
	movb (%rcx), %bl
	cmp $47, %bl
	jl rn_end		/* if less than '0' */
	cmp $57, %bl
	jg rn_end		/* if more than '9' */
	movq $0, %rdx
	imul $10, %rax
	subb $48, %bl
	addq %rbx, %rax
	addq $1, %rdi
	jmp rn_loop
rn_end:
	ret
