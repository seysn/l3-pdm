.data
buff:
	.space 10
number:
	.int 5
.text
.globl _start
_start:
	mov $buff, %r8
	mov %r8, %r11
	mov $number, %r10
	movw (%r10), %r12w

mkstr:
	mov $48, %r9
	add %r9, %r12
#	movb %r9b, %r11b

	add %r9, (%r11)
#	add $1, %r10
#	add $10, %r10
	
end:
	mov $4, %rax
	mov $1, %rbx
	mov %r8, %rcx
	mov $2, %rdx
	int $0x80

	mov $0, %rbx
	mov $1, %rax
	int $0x80
