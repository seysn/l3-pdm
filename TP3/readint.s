.data
rtr:
	.byte 10
buff:
	.space 10
.text
.globl _start
_start:
	mov $3, %rax
	mov $0, %rbx
	mov $buff, %rcx
	mov $10, %rdx
	int $0x80

	mov $buff, %r10
	movb (%r10), %r8b
	mov $48, %r9
	sub %r9, %r8

mknumber:
	add $1, %r10
	movb (%r10), %r11b
	
	mov $10, %r12b
	cmp %r11b, %r12b
	je end

	mov $48, %r9
	sub %r9, %r11

	mov $10, %r13
	imul %r13, %r8
	add %r11, %r8
	jmp mknumber

end:
	// r8
	mov $0, %rbx
	mov $1, %rax
	int $0x80
