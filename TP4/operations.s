.text
.globl _start
_start:
	movq $12, %rax
	push %rax
	movq $15, %rax
	push %rax
	call multiplication
	addq $16, %rbp
	push %rax
	jmp end
	
.type addition, @function
addition:	
	push %rbp
	movq %rsp, %rbp
	push %rbx
	movq 16(%rbp), %rbx
	movq 24(%rbp), %rax
	addq %rbx, %rax
	pop %rbx
	movq %rbp, %rsp
	pop %rbp
	ret

.type substract, @function
substract:
	push %rbp
	movq %rsp, %rbp
	push %rbx
	movq 16(%rbp), %rbx
	movq 24(%rbp), %rax
	subq %rbx, %rax
	pop %rbx
	movq %rbp, %rsp
	pop %rbp
	ret

.type multiplication, @function
multiplication:
	push %rbp
	movq %rsp, %rbp
	push %rbx
	movq 16(%rbp), %rbx
	movq 24(%rbp), %rax
	imul %rbx, %rax
	pop %rbx
	movq %rbp, %rsp
	pop %rbp
	ret

/*
.type division, @function
division:
	push %rbp
	movq %rsp, %rbp
	push %rbx
	movq 16(%rbp), %rbx
	movq 24(%rbp), %rax
	idiv %rbx, %rax
	pop %rbx
	movq %rbp, %rsp
	pop %rbp
	ret
*/
	
read:	
	mov $3, %rax
	mov $0, %rbx
	mov $buff, %rcx
	mov $10, %rdx
	int $0x80

end:
	mov $0, %rbx
	mov $1, %rax
	int $0x80
