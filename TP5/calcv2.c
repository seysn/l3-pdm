#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>

int64_t addition(int64_t, int64_t);
int64_t substract(int64_t, int64_t);
int64_t multiplication(int64_t, int64_t);
int64_t divide(int64_t, int64_t);

void empiler(int64_t);
int64_t depiler(void);

int main(void) {
	char str[80], tmp[80];
	char * pch;
	int a, b, l;
	
	/* Get input */
	scanf("%79s", str); 
	strcpy(tmp, str);

	/* Spliting & Taking values */
	pch = strtok(str, "+-*/");
	a = atoi(pch);
	l = strlen(pch);
	pch = strtok(NULL, "+-*/");
	b = atoi(pch);

	/* Search for the operand */
	switch (tmp[l]) {
	case '+':
		printf("%d + %d = %ld\n", a, b, addition(a, b));
		break;
	case '-':
		printf("%d - %d = %ld\n", a, b, substract(a, b));
		break;
	case '*':
		printf("%d * %d = %ld\n", a, b, multiplication(a, b));
		break;
	case '/':
		printf("%d / %d = %ld\n", a, b, divide(a, b));
		break;
	}

	return 0;
}
