	.text
	.globl addition
	.type addition, @function
addition:
	mov  %rdi, %rax
	addq %rsi, %rax
	ret

	.globl substract
	.type substract, @function
substract:
	mov  %rdi, %rax
	subq %rsi, %rax
	ret

	.globl multiplication
	.type multiplication, @function
multiplication:
	mov  %rdi, %rax
	imul %rsi, %rax
	ret

	.globl divide
	.type divide, @function
divide:
	movq $0, %rdx
	mov  %rdi, %rax
	idiv %rsi
	ret

	.globl empiler
	.type empiler, @function
empiler:
	mov  %rdi, %r9
	push %r9
	ret

	.globl depiler
	.type depiler, @function
depiler:
	pop %r9
	mov %r9, %rax
	ret
